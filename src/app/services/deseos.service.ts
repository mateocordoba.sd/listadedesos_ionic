import { Injectable } from '@angular/core';
import { Lista } from '../models/lista-model';


@Injectable({
  providedIn: 'root'
})
export class DeseosService {
  listas:any[]=[];

  constructor() {
    this.getStorage();


    
  }

  newList(tittle:string){
    const newList =new Lista(tittle);
    this.listas.push(newList);
    this.guardarStorage();
    return newList.id
  }

  guardarStorage(){
    
    localStorage.setItem('listas',JSON.stringify(this.listas) )
    console.log("GUARDANDO STORAGE");
    
    //this.getStorage();

  }

  getStorage(){
    if(localStorage.getItem('listas')){

      this.listas = JSON.parse(localStorage.getItem('listas'));
      //console.log(this.listas);
      
    }
    
    
    
  }

  getList(id:string | number){
    id = Number(id);
    //console.log(id);
    
    //console.log(this.listas.find(lista => lista.id === id))
    return this.listas.find(lista => lista.id === id)

  }

}
