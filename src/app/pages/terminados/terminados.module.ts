import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './terminados.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import {TerminadosRoutingModule } from './terminados-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    TerminadosRoutingModule
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
