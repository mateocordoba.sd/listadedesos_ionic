import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Lista } from 'src/app/models/lista-model';
import { DeseosService } from 'src/app/services/deseos.service';
import { Router } from "@angular/router"

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  listas:Lista[]=[];

  newList:any ;

  constructor(
      private deseosService:DeseosService,
      private router:Router,
      private alertctrl:AlertController
    ) {

    this.listas = this.deseosService.listas;

    this.newList = new FormGroup({
      titulo:new FormControl('', Validators.required)
    }) 
    

  }

  async agregarLista(){
    const alert = await this.alertctrl.create({
      header:'',
      inputs:[
        {
          name:'titulo',
          type: 'text',
          placeholder:'Nombre de la lista'

        }
      ],
      buttons:[
        {
          text: 'Cancelar',
          role:'cancel' 
        },
        {
          text:'Agregar',
          handler:(data)=>{

            if(data.titulo.length === 0){
              return;

            }

            const listaId = this.deseosService.newList(data.titulo);
            this.router.navigateByUrl(`/tab1/agregar/${listaId}`);

          }
          
        },
    ]

    });
    alert.present();
  }
  detalleLista(id:number){
    this.router.navigateByUrl(`/tab1/agregar/${id}`)
  }

}
