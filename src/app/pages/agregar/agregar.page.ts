import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListaItem } from 'src/app/models/lista-item-model';

import { Lista } from 'src/app/models/lista-model';

import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  lista:Lista;
  nombreItem:string;

  constructor(private deseosService: DeseosService, private router:ActivatedRoute) {
    const listaId =router.snapshot.paramMap.get('listaId');
    this.lista = this.deseosService.getList(listaId);
    

   }

  ngOnInit() {
  }

  agregarItem(){
    const newItem = new ListaItem(this.nombreItem);
    this.lista.items.push(newItem);
    this.deseosService.guardarStorage();
    this.nombreItem ='';
  }
  
  borrarItem(indice:number){
    this.lista.items.splice(indice, 1);
    this.deseosService.guardarStorage();   
    
    
    
  }
  cambioCheck(item:ListaItem){

    const pendientes = this.lista.items.filter(item => !item.completado);
    if(pendientes.length === 0){
      this.lista.terminada =true;
      this.lista.terminadaEn = new Date();
    }else{
      this.lista.terminada =false;
      this.lista.terminadaEn = null;
    }
    
    
    this.deseosService.guardarStorage();   
    
    

  }

}
