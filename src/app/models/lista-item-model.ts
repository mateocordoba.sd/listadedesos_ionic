export class ListaItem{
    titulo:string;
    completado:boolean;

    constructor(titulo){
        this.titulo = titulo;
        this.completado =false;
    }
}